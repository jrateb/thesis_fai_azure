#!/bin/bash


cd notebooks/
git clone https://jrateb@bitbucket.org/jrateb/thesis_fai_azure.git
conda create -y -n fastai2 python=3.7
conda activate fastai2
pip3 install fastai
cd thesis_fai_azure/

python -m fastai.launch imdb_64.py
python -m fastai.launch imdb_64.py
python -m fastai.launch imdb_64.py

python -m fastai.launch imagenette_64.py
python -m fastai.launch imagenette_64.py
python -m fastai.launch imagenette_64.py